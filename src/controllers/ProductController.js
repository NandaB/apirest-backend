const Product = require('../models/Product');


async function create(req,res){ //Cria uma instância nova do objeto 
    try{
        const Product =  await Product.create(req.body);
        return res.status(201).json({message: 'Produto cadastrado com sucesso!', Product: product});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function show (req,res){ //Retorna informação de uma instância específica 
    const {id} = req.params;
    try{
        const Product =  await Product.findByPk(id);
        return res.status(200).json({Product});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function addClient (req,res){ //adiciona a relação de uma instância de um objeto com uma instância específica de outro objeto
    const {clientId,requestId, productId} = req.params;
    try{
        const Client =  await Client.findByPk(clientId);
        const Request =  await Request.findByPk(requestId);
        const Product =  await Product.findByPk(productId);
        await Request.setClient(client);
        await Product.setRequest(request);
        return res.status(200).json(Product);
    }catch(err) {
        return res.status(500).json({err});
    }
};

async function removClient (req,res){ //Remove a relação de uma instância específica
    const {clientId,requestId, productId} = req.params;
    try{
        const Client =  await Client.findByPk(clientId);
        const Request =  await Request.findByPk(requestId);
        const Product =  await Product.findByPk(productId);
        await Request.setClient(null);
        await Product.setRequest(null);
        return res.status(200).json(null);
    }catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    create,
    show,
    addClient,
    removClient
};