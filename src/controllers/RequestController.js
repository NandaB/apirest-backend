const Request = require('../models/Request');

async function create(req,res){ //Cria uma instância nova do objeto 
    try{
        const Request =  await Request.create(req.body);
        return res.status(201).json({message: ' Pedido cadastrado com sucesso!', Request: request});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function index (req,res){ //Retorna todas as instâncias de um objeto 
    try{
        const Request =  await Request.findAll();
        return res.status(200).json({Request});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function show (req,res){ //Retorna informação de uma instância específica 
    const {id} = req.params;
    try{
        const Request =  await Request.findByPk(id);
        return res.status(200).json({Request});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function update (req,res){ //Edita uma instância específica 
    const {id} = req.params;

    try{
        const Request =  await Request.update(req.body,{where:{id: id}});
        if(update){
            const Request = await Request.findByPk(id);
            return res.status(200).json({Request});
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function destroy (req,res){ //Deleta uma instância específica
    const {id} = req.params;

    try{
        const Request =  await Request.destroy({where: {id: id}});
        if (deleted){
            return res.status(200).json("Pedido deletado com sucesso.");
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json("Pedido não encontrado");
    }
};



module.exports = {
    create,
    index,
    show,
    update,
    destroy
};