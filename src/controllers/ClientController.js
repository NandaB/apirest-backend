const Client = require('../models/Client');


async function create(req,res){ //Cria uma instância nova do objeto 
    try{
        const Client =  await Client.create(req.body);
        return res.status(201).json({message: 'Cliente cadastrado com sucesso!', Client: client});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function index (req,res){ //Retorna todas as instâncias de um objeto 
    try{
        const Client =  await Client.findAll();
        return res.status(200).json({Client});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function show (req,res){ //Retorna informação de uma instância específica 
    const {id} = req.params;
    try{
        const Client =  await Client.findByPk(id);
        return res.status(200).json({Client});
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function update (req,res){ //Edita uma instância específica 
    const {id} = req.params;

    try{
        const Client =  await Client.update(req.body,{where:{id: id}});
        if(update){
            const Client = await Client.findByPk(id);
            return res.status(200).json({Client});
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json(err);
    }
};

async function destroy (req,res){ //Deleta uma instância específica 
    const {id} = req.params;

    try{
        const Client =  await Client.destroy({where: {id: id}});
        if (deleted){
            return res.status(200).json("Cliente deletado com sucesso.");
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json("Cliente não encontrado");
    }
};



module.exports = {
    create,
    index,
    show,
    update,
    destroy
};