const Express = require('express');
const router = Express();
const ClientController = require('../controllers/ClientController');
const RequestController = require('../controllers/RequestController');
const ProductController = require('../controllers/ProductController');





//rotas para o cliente 
router.post('/client', ClientController.create);
router.get('/client/:id',ClientController.show);
router.get('/client',ClientController.index);
router.put('/client',ClientController.update);
router.delete('/client/:id',ClientController.destroy);

//rotas para o pedido
router.post('/request',RequestController.create);
router.get('/request/:id',RequestController.show);
router.get('/request',RequestController.index);
router.put('/request',RequestController.update);
router.delete('/request/:id',RequestController.destroy);


//rotas para o produto
router.post('/product',ProductController.create);
router.get('/product/:id',ProductController.show);



//rota para relação cliente-pedido-produto
router.put('/client/:clientId/request/resquestId/product/productId');
router.delete('/client/:clientId/request/resquestId/product/productId');



module.exports = router;
