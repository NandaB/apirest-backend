const Client = require("../../models/Client");
const faker = require('faker-br');

//função responsável de cidar o cliente

const seedClient = async function () {
    try{
        await Client.sync({force: true});//está sincronizando o BD

        for (let i=0;i<10;i++) {  //Fazem 10 criações de clientes com informações diferentes
            await Client.create({
                name: faker.name.firstName(),
                ultimoNome: faker.name.findName() ,
                email: faker.internet.email(),
                dataAniversário: faker.date.past(),
                telefone: faker.phone.number(),
                cidade: faker.address.cityName(),
                CEP: faker.address.zipCode(),
                numero: faker.address.buildingNumber(),
                senha: faker.internet.password(),
            });
        }
    } catch(err) {
        console.log(err);
    }
};

module.exports = seedClient;