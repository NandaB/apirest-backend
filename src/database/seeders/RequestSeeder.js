const Request = require("../../models/Request");
const faker = require('faker-br');

//função responsável de cidar o pedido

const seedRequest = async function () {
    try{
        await Request.sync({force: true});//está sincronizando o BD

        for (let i=0;i<10;i++) { //Fazem 10 criações de pedidos com informações diferentes
            await Request.create({
                
                dataChegada:faker.date.soon(),
                métodoPagamento: faker.finance.transactionType(),
                quantidade: faker.finance.amount(),
                dataEnvio: faker.date.between() ,
                valor: faker.commerce.price(),
                dataCompra: faker.date.between(),
                 
                
            });
        }
    } catch(err) {
        console.log(err);
    }
};

module.exports = seedRequest;