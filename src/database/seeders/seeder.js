require('../../config/dotenv')();
require('../../config/sequelize');

const seedClient = require('./ClienteSeeder');
const seedRequest = require('./RequestSeeder');
const seedProduct = require('./ProductSeeder');


(async () => {  //ela é executada apenas uma vez 
  try {
    await seedClient();
    await seedRequest();
    await seedProduct();


  } catch(err) { console.log(err) }
})();
