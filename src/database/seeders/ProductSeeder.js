const Product = require("../../models/Product");
const faker = require('faker-br');

//função responsável de cidar o produto 

const seedProduct = async function () {
    try{
        await Product.sync({force: true});//está sincronizando o BD

        for (let i=0;i<10;i++) {   //Fazem 10 criações de produtos com informações diferentes
            await Product.create({
                preço: faker.commerce.price(),
                descrição: faker.name.Brand(),
                modelo: faker.commerce.product(),
                cor: faker.commerce.color(),
            });
        }
    } catch(err) {
        console.log(err);
    }
};

module.exports = seedProduct;