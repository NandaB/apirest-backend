const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Request = sequelize.define('Request',{
    
    arrivaldate:{
        type: DataTypes.STRING,
        allowfull: false
    },
    paymentMethod:{
        type: DataTypes.STRING,
        allowfull: false
    },
    Amount:{
        type: DataTypes.INTEGER,
        allowfull: false
    },
    ShippingDate:{
        type: DataTypes.DATEONLY,
        allowfull: false
    },
    Value: {
        type: DataTypes.DOUBLE,
        allowfull: false
    },
   DateOfPurchase:{
    type: DataTypes.DATEONLY,
    allowfull: false
   },
}, {
    timestamps: false

});
  


Request.associate = function(models){
    Request.hasMany(models.Product);
    Request.belongsTo(models.Client);
};

module.exports = Request;