const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product',{
    Price: {
        type: DataTypes.DOUBLE,
        allowfull: false
    },
    Description:{
        type: DataTypes.STRING,
        allowfull: false
    },
    Model:{
        type: DataTypes.STRING,
        allowfull: false
    },
    Color:{
        type: DataTypes.STRING,
        allowfull: false
    }
}, {
    timestamps: false
});
  


Product.associate = function(models){
    Product.belongsTo(models.Client);   
    Product.belongsTo(models.Request);


};

module.exports = Product;
