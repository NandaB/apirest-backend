const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Client = sequelize.define('Client',{
   Name: {
    type: DataTypes.STRING,
    allowfull: false
   },
  LastName:{
    type: DataTypes.STRING,
    allowfull: false
  },
  Email:{
    type: DataTypes.STRING,
    allowfull: false
  },
  Date_of_birth:{
    type: DataTypes.DATEONLY,
    allowfull: false
  },
  Telephone:{
    type: DataTypes.STRING,
    allowfull: false
  },
  Town:{
    type: DataTypes.STRING,
    allowfull: false
  },
  CEP:{
    type: DataTypes.STRING,
    allowfull: false
  },
  Number:{
    type: DataTypes.STRING,
    allowfull: false
  },
  Passaword:{
    type: DataTypes.STRING,
    allowfull: false
  },

},{
  timestamps: false
});

Client.associate = function(models){
    Client.hasMany(models.Request);
    Client.hasMany(models.Product);

};

module.exports = Client;